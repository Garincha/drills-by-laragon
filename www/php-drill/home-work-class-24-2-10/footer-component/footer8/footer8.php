<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Footer8</title>
</head>
<?php
        $footer = <<<'PONDIT_JOHN'
<body>
    <img src="img/footer8.jpg">
    <footer>
        <div id="top-footer">
            <div id="footer-content">
                <h3>Footer Content</h3>
                <p>Here you can use rows and columns to organize your footer content.</p>

                <p>Lorem Ipsum dolor sit amet,consectetur adipisicing elit.Fugit amet numquam iure provident voluptate
                    esse quasi,veritatis totam voluptas nostrum.</p>
            </div>

            <div id="About">
                <h3>About</h3>
                <nav>
                    <ul>
                        <li><a href="#">Projects</a></li>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Awards</a></li>
                    </ul>
                </nav>
            </div>

            <div id="Address">
                <h3>Address</h3>
                <nav>
                    <ul>
                        <li><a href="#">New York,NY 10012 US</a></li>
                        <li><a href="#">info@gmail.com</a></li>
                        <li><a href="#">+0123456789</a></li>
                        <li><a href="#">+9985236469</a></li>
                    </ul>
                </nav>
            </div>

            <div id="Follow Us">
                <h3>Follow Us</h3>
                <nav>
                    <ul>
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Google plus</a></li>
                        <li><a href="#">Browser</a></li>
                    </ul>
                </nav>
            </div>
        </div>

        <div id="bottom-footer">
            <p>&copy;2018 Copyright:MDBootstrap.com</p>
        </div>

    </footer>
</body>
PONDIT_JOHN;
?>
<?php
echo $footer;
?>
</html>