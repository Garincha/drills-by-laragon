<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Header8</title>
</head>

<?php
        $header = <<<'PONDIT_JOHN'
<body>
    <img src="img/header8.jpg">

    <header>
        <div id="logo">
            <a href="#">amazon</a>

        </div>
        <div class="top-header">
            <nav>
                <ul>
                    <li>
                        <label for="car-brands"></label>
                        <select id="car-brands" name="car-brands">
                            <option value="all">All</option>
                        </select>
                        <label for="input-search-button"></label>
                        <input id="input-search-button" type="text" name="domain-name" value="" required placeholder="">
                    </li>
                    <li><a href="#">Shop Mother's Day Gifts</a></li>
        </div>

        <div class="bottom-header">
            <nav>
                <ul>
                    <li>
                        <label for="car-brands1"></label>
                        <select id="car-brands1" name="car-brands">
                            <option value="departments">Departments</option>
                        </select>
                    </li>
                    <li><a href="#">Your Amazon.com</a></li>
                    <li><a href="#">Todays Demand</a></li>
                    <li><a href="#">Gift Cards</a></li>
                    <li><a href="#">Registry</a></li>
                    <li><a href="#">Send</a></li>
                    <li><a href="#">Help</a></li>
                </ul>
            </nav>
        </div>
    </header>
</body>
PONDIT_JOHN;
?>
<?php
echo $header;
?>


</html>