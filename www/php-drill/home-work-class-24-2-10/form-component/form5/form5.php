<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>form5</title>
</head>
<?php
        $form = <<<'PONDIT_JOHN'
<body>
    <div>
        <img src="img/form5.jpg">
    </div>
    <h1>Registration Form For Sport</h1>
    <form action="#" method="post">

        <div>
            <label for="name">Name:</label>
            <input id="name" type="text" name="name" value="" required placeholder="">
        </div>
        <br>

        <div>
            <label for="sur-name">Sur Name:</label>
            <input id="sur-name" type="text" name="last-name" value="" required placeholder="">
        </div>
        <br>
        <div>
            <label for="date-of-birth">Date of Birth:</label>
            <input id="date-of-birth" type="date-of-birth" name="date-of-birth" value="" required placeholder="">
        </div>
        <br>
        <div>
            <label for="address">Address:</label>
            <input id="address" type="address" name="address" value="" required placeholder="">
        </div>
        <br>
        <div>
            <label for="phone">Phone:</label>
            <input id="phone" type="phone" name="phone" value="" required placeholder="">
        </div>
        <br>
        <div>
            <label for="email">Email:</label>
            <input id="email" type="email" name="email" value="" required placeholder="">
        </div>
        <br>
        <div>
            <label for="zip">Zip:</label>
            <input id="zip" type="zip" name="zip" value="" required placeholder="">
        </div>
        <br>
        <button type="register">Register</button>
    </form>
</body>
PONDIT_JOHN;
?>
<?php
echo $form;
?>
</html>