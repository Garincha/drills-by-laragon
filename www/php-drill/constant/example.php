<?php
    // Valid constant names
define("FOO",     "something");
define("FOO2",    "something else");
define("FOO_BAR", "something more");
echo FOO;
echo"<br>";
echo FOO2;
echo"<br>";
echo FOO_BAR;
echo"<br>";

// Invalid constant names
define("2FOO",    "something");
//echo 2FOO;

// This is valid, but should be avoided:
// PHP may one day provide a magical constant
// that will break your script
define("__FOO__", "something"); 
echo _FOO_;
?>