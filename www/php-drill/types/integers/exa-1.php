<?php
    $num = 1234; // decimal number
    echo $num;
    $num2 = 0123; // octal number (equivalent to 83 decimal)
    echo $num2;
    $num3 = 0x1A; // hexadecimal number (equivalent to 26 decimal)
    echo $num3;
    $num4 = 0b11111111; // binary number (equivalent to 255 decimal)
    echo $num4;
?>