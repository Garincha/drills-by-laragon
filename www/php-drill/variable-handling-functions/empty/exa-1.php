<?php

$var = 0;
$var2 = 34;
// Evaluates to true because $var is empty
if (empty($var)) {
    echo '$var is either 0, empty, or not set at all';
}
echo "<br>";
// Evaluates as true because $var is set
if (isset($var)) {
    echo '$var is set even though it is empty';
}
echo "<br>";

if(empty($var2)){
    echo"This variable is empty";
}
else{
    echo "This variable isn't empty";
}
?>